// Query Operators (Comparison)

// $gt / $gte - greater than / greater than or equal to

// syntax: 
// db.collectionName.find({ field:{ $gt: value } });
// db.collectionName.find({ field:{ $gte: value } });
db.users.find({ age: { $gt: 80 } });
db.users.find({ age: { $gte: 70 } });




// $lt (less than) / $lte (less than or equal to)
db.users.find({ age: { $lt: 80 } });

// $ne (not equal)
db.users.find({ age: { $ne: 70 } });

// $in
// syntax: db.collectionName.find({ field:{ $in: [value, ..., ...] } });
db.users.find({ "lastName": { $in: ["Hawking", "Gates", "Doe"] } }).pretty();


// Query Operators (Logical)

// $or
// db.collectionName.find({ $or: [{ fieldA: valueA }, { fieldB: valueB }] });
db.users.find({ $or: [{ "firstName": "Neil" }, { "age": 25 }] });


// $and
// db.collectionName.find({ $and: [{ fieldA: valueA }, { fieldB: valueB }] });
db.users.find({ $and: [{ "firstName": "Neil" }, { "age": { $gt: 50 } }] });



db.users.find({ 
	$and: 
		[
			{ "age": 
				{ $ne: 82 } 
			}, 
			{ "age": 
				{ $ne: 76 } 
			}
		] 
	}
);



// Field Projections


// Inclusion
// - Fields are included to resulting documents
// Syntax: 	db.collectionName.find({ filter }, { projections });
// 			db.collectionName.find({ field:value }, { field:1, [field: 1, ...] });
db.users.find({ "firstName": { $ne: "Jane" } }, { "firstName": 1, "lastName": 1 });



// Exclusion
// - Fields are excluded to the resulting documents
// Syntax: 	db.collectionName.find({ filter }, { projections });
// 			db.collectionName.find({ field:value }, { field:1 ,[field: 1, ...] });
db.users.find(
	{"firstName": {$ne: "Jane"}},
	{"_id": 0, "contact": 0, "department": 0}
);

// Inclusion/exclusion of nested fields
// Syntax: 	db.collectionName.find({ field: value }, { field: 1 , field.nestedField: 1, ... });
// 			db.collectionName.find({ field: value }, { field: 0 , field.nestedField: 0, ... });

db.users.find(
	{"age": {$gt: 75}}, {
		"firstName": 1, 
		"lastName": 1, 
		"contact.phone": 1
	}
);


// Query Operator (Evaluation)

// $regex --> patterns
// syntax: db.collectionName.find({ field: $regex: 'pattern', $options: '$optionValue' });


// Case Sensitive
db.users.find(
	{ "firstName": { $regex: "N" } }
);


// Case Insensitive
db.users.find(
	{ "firstName": { $regex: "N", $options: '$i' } }
);